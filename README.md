## Installation

```bash
$ yarn install
```

## Running the app

```bash
# start db and service
$ yarn docker:env:up

# finish
$ yarn docker:env:down
```

## Open API Swagger
http://localhost:3000/api

### Endpoint
http://localhost:3000/api/users/getAll/{page}
#### Page
page > 0
#### Perpage
1 < PerPage <= 50
#### Search Format
```email=test@mail.ru,name=Nikola```

## Migration
```bash
# create migration file
$ yarn file-migration

# migration
$ yarn migration
```