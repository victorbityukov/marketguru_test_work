import { Table, Column, Model, BeforeCreate, BeforeUpdate } from 'sequelize-typescript';
import { hash } from 'bcrypt';

@Table({
  tableName: 'users',
  createdAt: false,
  updatedAt: false,
})
export class UserEntity extends Model {
  @Column({
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  email: string;

  @Column({
    unique: true,
    type: 'varchar',
  })
  phone: string;

  @Column
  password: string;

  @BeforeCreate
  static async hashPasswordBeforeCreate(user: UserEntity) {
    if (user.password) {
      user.password = await hash(user.password, 10);
    }
  }

  @BeforeUpdate
  static async hashPasswordBeforeUpdate(user: UserEntity) {
    if (user.password) {
      user.password = await hash(user.password, 10);
    }
  }
}