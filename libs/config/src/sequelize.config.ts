import { SequelizeModuleOptions } from '@nestjs/sequelize';
import { UserEntity } from '@app/dbo';
import { DbConfig } from '@app/config/db.config';

export const DB_ENTITIES = [
  UserEntity,
];

const {
  POSTGRES_HOST,
  POSTGRES_DB,
  POSTGRES_PASSWORD,
  POSTGRES_PORT,
  POSTGRES_USER
} = DbConfig;

export const dbConfiguration: SequelizeModuleOptions = {
  dialect: 'postgres',
  host: POSTGRES_HOST,
  port: POSTGRES_PORT,
  username: POSTGRES_USER,
  password: POSTGRES_PASSWORD,
  database: POSTGRES_DB,
  models: DB_ENTITIES,
  synchronize: true,
  autoLoadModels: true,
};
