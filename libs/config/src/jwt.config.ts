import { get } from 'env-var';

export class TestServiceConfig {
  public static readonly TEST_SERVICE_CONTAINER_NAME: string = get(
    'TEST_SERVICE_CONTAINER_NAME',
  )
    .required()
    .asString();

  public static readonly TEST_SERVICE_BASE_PORT: number = get(
    'TEST_SERVICE_BASE_PORT',
  )
    .required()
    .asPortNumber();
}
