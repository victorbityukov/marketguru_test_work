import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerDocumentOptions, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { TestServiceConfig } from '@app/config';
import { AppModule } from './app.module';
import { UsersModule } from './users';

async function bootstrap() {
  const {TEST_SERVICE_BASE_PORT} = TestServiceConfig;
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const config = new DocumentBuilder()
    .setTitle('Test Marketguru')
    .setDescription('API description')
    .setVersion('0.999')
    .build();

  const options: SwaggerDocumentOptions =  {
    include: [UsersModule],
    deepScanRoutes: true
  };

  const document = SwaggerModule.createDocument(app, config, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(TEST_SERVICE_BASE_PORT);
}
bootstrap();
