import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { dbConfiguration } from '@app/config';
import { UsersModule } from './users';

@Module({
  imports: [
    SequelizeModule.forRoot(dbConfiguration),
    UsersModule,
  ],
})
export class AppModule {}
