import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_USER_ACCESS_TOKEN: ApiPropertyOptions = {
  name: 'accessToken',
  description: 'User ACCESS TOKEN',
  type: String,
};

export const SWAGGER_USER_NAME: ApiPropertyOptions = {
  name: 'name',
  description: 'User name',
  type: String,
  required: false,
  example: 'Vasiliy',
};

export const SWAGGER_USER_EMAIL: ApiPropertyOptions = {
  name: 'email',
  description: 'User email',
  type: String,
  required: false,
  example: 'example@mail.com',
};

export const SWAGGER_USER_PHONE: ApiPropertyOptions = {
  name: 'phone',
  description: 'User phone',
  type: String,
  required: false,
  example: '7999999999',
};

export const SWAGGER_USER_PASSWORD: ApiPropertyOptions = {
  name: 'password',
  description: 'User password',
  type: String,
  required: false,
  example: 'koshka',
};
