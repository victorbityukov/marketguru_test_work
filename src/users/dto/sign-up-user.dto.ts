import { IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_USER_NAME,
  SWAGGER_USER_PASSWORD,
} from '../swagger';
import { PhoneUserDto } from './phone-user.dto';
import { EmailUserDto } from './email.user.dto';

export class SignUpUserDto {
  @ApiProperty(SWAGGER_USER_NAME)
  @IsString()
  readonly name: string;

  @ApiProperty(SWAGGER_USER_PASSWORD)
  @IsString()
  readonly password: string;

  @ApiProperty({
    oneOf: [
      {
        type: 'object',
        required: ['phone'],
        properties: {
          phone: { type: 'string' },
        },
      },
      {
        type: 'object',
        required: ['email'],
        properties: {
          email: { type: 'string' } },
      },
    ],
  })
  @ValidateNested()
  @Type((type) => (type.object.phoneOrEmail.email ? EmailUserDto : PhoneUserDto))
  readonly phoneOrEmail: PhoneUserDto | EmailUserDto;
}