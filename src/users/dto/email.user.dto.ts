import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_USER_EMAIL } from '../swagger';

export class EmailUserDto {
  @ApiProperty(SWAGGER_USER_EMAIL)
  @IsEmail()
  readonly email: string;
  readonly phone?: string;
}