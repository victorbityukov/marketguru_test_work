import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_USER_EMAIL,
  SWAGGER_USER_NAME,
  SWAGGER_USER_PASSWORD,
  SWAGGER_USER_PHONE
} from '../swagger';

export class CreateUserDto {
  @ApiProperty(SWAGGER_USER_NAME)
  @IsString()
  readonly name: string;

  @ApiProperty(SWAGGER_USER_EMAIL)
  @IsEmail()
  readonly email: string;

  @ApiProperty(SWAGGER_USER_PASSWORD)
  @IsString()
  readonly password: string;

  @ApiProperty(SWAGGER_USER_PHONE)
  @IsString()
  readonly phone: string;
}