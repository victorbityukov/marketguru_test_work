import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_USER_PHONE } from '../swagger';

export class PhoneUserDto {
  @ApiProperty(SWAGGER_USER_PHONE)
  @IsString()
  readonly phone: string;
  readonly email?: string;
}