import { IsEmail, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_USER_EMAIL,
  SWAGGER_USER_NAME,
  SWAGGER_USER_PASSWORD,
  SWAGGER_USER_PHONE
} from '../swagger';

export class UpdateUserDto {
  @ApiProperty(SWAGGER_USER_NAME)
  @IsString()
  @IsOptional()
  readonly name: string;

  @ApiProperty(SWAGGER_USER_EMAIL)
  @IsEmail()
  @IsOptional()
  readonly email: string;

  @ApiProperty(SWAGGER_USER_PASSWORD)
  @IsString()
  @IsOptional()
  readonly password: string;

  @ApiProperty(SWAGGER_USER_PHONE)
  @IsString()
  @IsOptional()
  readonly phone: string;
}