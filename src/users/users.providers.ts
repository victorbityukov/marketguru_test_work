import { UserEntity } from '@app/dbo';

export const usersProviders = [
  {
    provide: 'USER_REPOSITORY',
    useValue: UserEntity,
  },
];
