import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DB_ENTITIES, JwtConfig } from '@app/config';
import { UsersService } from './users.service';
import { usersProviders } from './users.providers';
import { UsersController } from './users.controller';

@Module({
  imports: [
    SequelizeModule.forFeature(DB_ENTITIES),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async () => ({
        secret: JwtConfig.JWT_SECRET,
        signOptions: {
          expiresIn: JwtConfig.JWT_EXPIRATION_TIME,
        },
      }),
    }),
  ],
  providers: [UsersService, ...usersProviders,],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule {}