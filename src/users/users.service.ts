import { Repository } from 'sequelize-typescript';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Injectable, Inject, BadRequestException, NotFoundException } from '@nestjs/common';
import { UserEntity } from '@app/dbo';
import { JwtConfig } from '@app/config';
import { CreateUserDto, SignInUserDto, SignUpUserDto, UpdateUserDto } from './dto';
import { JwtPayloadDto } from './jwt';

@Injectable()
export class UsersService {
  constructor(
    @Inject('USER_REPOSITORY')
    private readonly usersRepository: Repository<UserEntity>,
    private jwtService: JwtService) {
  }

  async create(userData: CreateUserDto): Promise<UserEntity> {
    return this.usersRepository.create({ ...userData });
  }

  async findAll(): Promise<UserEntity[]> {
    return this.usersRepository.findAll();
  }

  async findOne(id: number): Promise<UserEntity> {
    return this.usersRepository.findByPk(id);
  }

  async update(id: number, userData: UpdateUserDto) {
    await this.usersRepository.update({ ...userData }, { where: { id }, individualHooks: true });
  }

  async remove(id: number) {
    await this.usersRepository.destroy({ where: { id } });
  }

  async getAll(page: number = 1, perPage: number = 10, search: string = '') {
    const fieldsForSearch = ['name', 'email'];
    if (page < 1) {
      page = 1;
    }
    if (perPage < 1) {
      perPage = 1;
    } else if (perPage > 50) {
      perPage = 50;
    }

    const conditionSearch = {};
    search
      .split(',')
      .forEach((item) => {
        const [key, value] = item.split('=');
        if (fieldsForSearch.indexOf(key) !== -1) {
          conditionSearch[key] = value;
        }
      });

    const offset = (page - 1) * perPage;
    const { count, rows } = await this.usersRepository.findAndCountAll(
      {
        where: conditionSearch,
        distinct: true,
        order: [['id', 'ASC']],
        offset,
        limit: perPage
      });
    return { count, rows };
  }

  async signUp(userData: SignUpUserDto): Promise<UserEntity> {
    const { name, password } = userData;
    let login: { [key: string]: string } = {};

    if (!!userData.phoneOrEmail?.email) {
      login['email'] = userData.phoneOrEmail.email;
    } else {
      if (!!userData.phoneOrEmail?.phone) {
        login['phone'] = userData.phoneOrEmail.phone;
      }
    }
    const recordUser = {
      name,
      password,
      ...login
    };

    return this.usersRepository.create({ ...recordUser });
  }

  async signIn(userData: SignInUserDto) {
    const { password } = userData;
    let login: { [key: string]: string } = {};
    if (!!userData.phoneOrEmail?.email) {
      login['email'] = userData.phoneOrEmail.email;
    } else {
      if (!!userData.phoneOrEmail?.phone) {
        login['phone'] = userData.phoneOrEmail.phone;
      }
    }
    const user = await this.usersRepository.findOne({ where: { ...login } });
    if (!user) {
      throw new NotFoundException('User is not found');
    }
    const isCompared: boolean = await compare(password, user.password);
    if (!isCompared) {
      throw new BadRequestException('Wrong login or password');
    }
    const accessToken = await this.getAccessToken({name: user.name});
    return { accessToken }
  }

  async getAccessToken(payload: JwtPayloadDto) {
    return this.jwtService.sign(payload, {
      secret: JwtConfig.JWT_SECRET,
      expiresIn: JwtConfig.JWT_EXPIRATION_TIME,
    });
  }
}